//Caminho para o JSON que popula a interface
let jsonPath = 'https://my-json-server.typicode.com/matmpimentel/commcepta/profiles';
let selected = 0;


//Fazemos o fetch async do JSON e guardamos esses dados numa variável "profiles"
(async () => {
  const profilesResponse = await fetch(jsonPath);
  const profiles = await profilesResponse.json();


	//Criamos uma nova instância VUE em #app
  const app = new Vue({
    el: '#app',
    data : function() {
      return {
        profiles : profiles,
        currentProfile : profiles[selected],
        activeIndex: null

      }
    },
    //Método para troca da informação no box dinamico.
    methods: {
    	toggle:   function toggle(selected, index){
    						this.activeIndex = index;
								this.currentProfile = selected;
    	}
    }
  });
  


})()
setTimeout (function()
{document.getElementById("boxContainer").classList.remove("bodytransition")}, 350

);